function freeze() {
        if($("html").css("position") != "fixed") {
            var top = $("html").scrollTop() ? $("html").scrollTop() : $("body").scrollTop();
            if(window.innerWidth > $("html").width()) {
                $("html").css("overflow-y", "scroll");
            }
            $("html").css({"width": "100%", "height": "100%", "position": "fixed", "top": -top});
        }
}

function unfreeze() {
        if($("html").css("position") == "fixed") {
            $("html").css("position", "static");
            $("html, body").scrollTop(-parseInt($("html").css("top")));
            $("html").css({"position": "", "width": "", "height": "", "top": "", "overflow-y": ""});
        }
}

$(document).ready(function() 
{
    $(document).foundation();
    $(".header").headroom({
      tolerance: {
            up: 0,
            down: 0
        },
        offset: 0,
        classes: {
            pinned: "headroom--pinned",
            unpinned: "headroom--unpinned",
            top: "headroom--top",
            notTop: "headroom--not-top",
            initial: "headroom"
        }
    });
    $(".owl-carousel").owlCarousel(
    {
  	    center: true,
        touchDrag: true,
        mouseDrag: true,
        dots: true,
        loop: true,
        dotsEach: 1,
        responsiveClass: true,
        lazyLoad: true,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            640: {
                items: 3
            },
            1024: {
                items: 5
            }}
      })

      $(".showVideosButton").click(function()
      {
          $(".secondLine").css({
            "display": "block", 
        opacity          : 1,
        WebkitTransition : 'opacity 1s ease-in-out',
        MozTransition    : 'opacity 1s ease-in-out',
        MsTransition     : 'opacity 1s ease-in-out',
        OTransition      : 'opacity 1s ease-in-out',
        transition       : 'opacity 1s ease-in-out'
           });
          $(".secondLine").fadeIn();
          $(".showVideosButton").css("display", "none");
      })

      $('.hamburger').click(function()
      {
          $("#nav-icon4").toggleClass('open');
      })
      
      $('.smallMenuTitle').click(function()
      {
          $("#nav-icon4").toggleClass('open');
      })
      var vidsrc = $("iframe").attr('src');
      $('.reveal-overlay').click(function()
      {
          $(".reveal-overlay").css("display", "none");
          $("iframe").css("display", "none");
          $("iframe").attr("src", '');
          $("iframe").attr("src", vidsrc);
      })

      $('.close-button').click(function()
      {
          $(".reveal-overlay").css("display", "none");
      })
      $('.yuriy').click(function()
      {
          $(".reveal-overlay").css("display", "block");
          $("iframe").css("display", "block");
      })
      $(".smallMenu").click(function()
      {
          if ($("#nav-icon4").hasClass("open"))
          {
              $(".menu").css("display", "block")
              freeze()
          }
          else
          {
              unfreeze()
              $(".menu").css("display", "none"); 
          }
      })
});